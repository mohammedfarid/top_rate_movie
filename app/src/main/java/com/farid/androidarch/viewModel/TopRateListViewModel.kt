package com.farid.androidarch.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.farid.androidarch.apiHandler.BaseApiCall
import com.farid.androidarch.model.GenersResponse
import com.farid.androidarch.model.MovieListTopRateResponse

class TopRateListViewModel(application: Application) : AndroidViewModel(application) {
    var movieListTopRateLiveData: LiveData<MovieListTopRateResponse>? = null
    var genreListLiveData: LiveData<GenersResponse>? = null

    init {
        movieListTopRateLiveData = BaseApiCall.getMoviesListTopRatedCall()
        genreListLiveData = BaseApiCall.getListCall()
    }

    fun getMovieTopRated (): LiveData<MovieListTopRateResponse>? {
        return movieListTopRateLiveData
    }

    fun getGener():LiveData<GenersResponse>?{
        return genreListLiveData
    }
}