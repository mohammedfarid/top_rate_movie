package com.farid.androidarch.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.farid.androidarch.apiHandler.BaseApiCall
import com.farid.androidarch.model.MovieDetails

class MovieDetailsViewModel(application: Application) : AndroidViewModel(application) {
    var movieDetailsLiveData: LiveData<MovieDetails>? = null

    init {

    }

    fun getMovieDetails(movieId: String): LiveData<MovieDetails>? {
        movieDetailsLiveData = BaseApiCall.getMovieDetailsCall(movieId)
        return movieDetailsLiveData
    }
}