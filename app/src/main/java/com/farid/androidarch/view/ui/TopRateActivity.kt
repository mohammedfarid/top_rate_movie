package com.farid.androidarch.view.ui

import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import com.farid.androidarch.R

class TopRateActivity : AppCompatActivity() {
    var actionBar: ActionBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_rate)

        actionBar = supportActionBar
        actionBar!!.title = "Top Rate Movie"

        val topRatedFragment = TopRatedFragment()
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.add(R.id.fragment, topRatedFragment, "tag")
        transaction.commit()
    }
}
