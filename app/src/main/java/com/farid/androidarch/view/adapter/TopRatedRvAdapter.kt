package com.farid.androidarch.view.adapter

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.farid.androidarch.App
import com.farid.androidarch.R
import com.farid.androidarch.model.GenersResponse
import com.farid.androidarch.model.MovieListTopRateResponse
import com.farid.androidarch.view.ui.MovieDetailsFragment
import com.farid.androidarch.view.ui.TopRateActivity
import com.squareup.picasso.Picasso


class TopRatedRvAdapter(
    val context: Context,
    var topRateActivity: TopRateActivity,
    private var moviesList: MovieListTopRateResponse?,
    private var genreList: GenersResponse?
) : RecyclerView.Adapter<TopRatedRvAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_movie_list, p0, false))
    }

    override fun getItemCount(): Int {
        return moviesList!!.results!!.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val MOVIE_ID = "movie_id"
        val MOVIE_NAME = "movie_name"
        var string: String = ""
        var stringComa:String = ""
        p0.title.text = moviesList!!.results!![p1]!!.title
        Picasso.get().load(App.API_IMAGE_URL + moviesList!!.results!![p1]!!.poster_path).into(p0.imagemovie)
        if (genreList != null) {
            for (i in genreList!!.genres!!) {
                for (j in moviesList!!.results!![p1]!!.genre_ids!!) {
                    if (i!!.id == j) {
                        string += stringComa + i!!.name
                        stringComa = ","
                        p0.gener.text = string
                    }
                }
            }
        }
        p0.view.setOnClickListener {
            val movieDetailsFragment = MovieDetailsFragment()
            val args = Bundle()
            args.putString(MOVIE_ID, moviesList!!.results!![p1]!!.id.toString())
            args.putString(MOVIE_NAME, moviesList!!.results!![p1]!!.title)
            movieDetailsFragment.arguments = args
            topRateActivity.supportFragmentManager.beginTransaction()
                .add(R.id.fragment, movieDetailsFragment, "tag")
                .addToBackStack(null).commit()
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imagemovie: ImageView = view.findViewById(R.id.movie_iv)
        val title: TextView = view.findViewById(R.id.movie_tv)
        val gener: TextView = view.findViewById(R.id.movie_tv_gener)
        val view: View = view
    }
}