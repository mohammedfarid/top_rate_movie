package com.farid.androidarch.view.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.farid.androidarch.R
import com.farid.androidarch.model.GenersResponse
import com.farid.androidarch.model.MovieListTopRateResponse
import com.farid.androidarch.view.adapter.TopRatedRvAdapter
import com.farid.androidarch.viewModel.TopRateListViewModel
import kotlinx.android.synthetic.main.fragment_top_rated.*


class TopRatedFragment : Fragment() {
    private var topRateActivity: TopRateActivity? = null
    private var topRateListViewModel: TopRateListViewModel? = null
    private var listGener: GenersResponse? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    private var topRatedRvAdapter: TopRatedRvAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        topRateActivity = activity as TopRateActivity?
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_top_rated, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        linearLayoutManager = LinearLayoutManager(activity!!.applicationContext)
        linearLayoutManager!!.orientation = LinearLayoutManager.VERTICAL
        rvRec.layoutManager = linearLayoutManager

        topRateListViewModel = ViewModelProviders.of(this).get(TopRateListViewModel::class.java)

        topRateListViewModel!!.getGener()!!.observe(this,
            Observer<GenersResponse> { t ->
                if (t != null) {
                    listGener = t
                }
            })

        topRateListViewModel!!.getMovieTopRated()!!.observe(this,
            Observer<MovieListTopRateResponse> { t ->
                if (t != null) {
                    topRatedRvAdapter =
                            TopRatedRvAdapter(activity!!.applicationContext, (activity as TopRateActivity?)!!, t, listGener)
                    rvRec.adapter = topRatedRvAdapter

                }
            })
    }


}
