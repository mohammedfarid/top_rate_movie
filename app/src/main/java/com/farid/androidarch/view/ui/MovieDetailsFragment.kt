package com.farid.androidarch.view.ui


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.farid.androidarch.App

import com.farid.androidarch.R
import com.farid.androidarch.model.MovieDetails
import com.farid.androidarch.viewModel.MovieDetailsViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_movie_details.*


private const val MOVIE_ID = "movie_id"
private const val MOVIE_NAME = "movie_name"

class MovieDetailsFragment : Fragment() {
    var topRateActivity: TopRateActivity? = null
    var movieId: String? = ""
    var movieName: String? = ""
    var movieDetailsViewModel: MovieDetailsViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        topRateActivity = activity as TopRateActivity?
        arguments?.let {
            movieId = it.getString(MOVIE_ID)
            movieName = it.getString(MOVIE_NAME)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as TopRateActivity).supportActionBar!!.title = movieName
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        movieDetailsViewModel = ViewModelProviders.of(this).get(MovieDetailsViewModel::class.java)

        movieDetailsViewModel!!.getMovieDetails(movieId.toString())!!.observe(this,
            Observer<MovieDetails> { t ->
                var string: String = ""
                var stringComa: String = ""
                if (t != null) {
                    Picasso.get().load(App.API_IMAGE_URL_V2 + t.poster_path).into(imagePoster)
                    voteRate.text = t.vote_average.toString()
                    voteCount.text = t.vote_count.toString()
                    releaseDate.text = t.release_date.toString().reversed()
                    originalLanguage.text = t.original_language
                    for (i in t.genres!!) {
                        string += stringComa + i!!.name
                        stringComa = ","
                        genre.text = string
                    }
                }
            })
    }

}
