package com.farid.androidarch

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo


class App : Application() {

    companion object {
        val API_BASE_URL = "https://api.themoviedb.org/3/"
        var API_KEY: String? = "7211bbf86520c862f36e6e6babfc8db2"
        val INCLUDE_ADULT = "false"
        val SORT_BY = "popularity.desc"
        val API_IMAGE_URL = "http://image.tmdb.org/t/p/w300/"
        val API_IMAGE_URL_V2 = "https://image.tmdb.org/t/p/w600_and_h900_bestv2/"

        var instance: App? = null

        fun hasNetwork(): Boolean {
            return instance!!.checkIfHasNetwork()
        }

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }


    fun getInstance(): App? {
        return instance
    }

    fun checkIfHasNetwork(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo: NetworkInfo? = cm.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

}