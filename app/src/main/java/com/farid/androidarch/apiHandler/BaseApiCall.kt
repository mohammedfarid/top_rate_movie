package com.farid.androidarch.apiHandler

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.farid.androidarch.App
import com.farid.androidarch.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class BaseApiCall {
    companion object {
        private var baseApi: BaseApi? = null

        init {
            if (baseApi == null)
                baseApi = BaseApiHandler.setupBaseApi()!!.create(BaseApi::class.java)
        }

        private var disposable: Disposable? = null

        fun getListCall(): LiveData<GenersResponse> {
            val genreListLiveData: MutableLiveData<GenersResponse> = MutableLiveData()
            disposable = baseApi!!.getList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    genreListLiveData.value = result
                }, {
                    genreListLiveData.value = null
                })
            return genreListLiveData
        }

        fun getMoviesListTopRatedCall(): LiveData<MovieListTopRateResponse> {
            val moviesListLiveData: MutableLiveData<MovieListTopRateResponse> = MutableLiveData()
            disposable = baseApi!!.getMovieTopRated()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    moviesListLiveData.value = result
                }, {
                    moviesListLiveData.value = null
                })
            return moviesListLiveData
        }

        fun getMovieDetailsCall(moviesId: String): LiveData<MovieDetails> {
            val movieDetailsLiveData: MutableLiveData<MovieDetails> = MutableLiveData()
            disposable = baseApi!!.getMovieDetails(moviesId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    movieDetailsLiveData.value = result
                }, {
                    movieDetailsLiveData.value = null
                })
            return movieDetailsLiveData
        }
    }
}