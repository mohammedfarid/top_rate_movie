package com.farid.androidarch.apiHandler


import com.farid.androidarch.model.GenersResponse
import com.farid.androidarch.model.MovieDetails
import com.farid.androidarch.model.MovieListTopRateResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Ahmed Moharm on 4/29/2018.
 */

interface BaseApi {
    //https://api.themoviedb.org/3/genre/movie/list?api_key=7211bbf86520c862f36e6e6babfc8db2
    @GET("genre/movie/list?api_key=7211bbf86520c862f36e6e6babfc8db2")
    fun getList(): Observable<GenersResponse>

    @GET("movie/top_rated?api_key=7211bbf86520c862f36e6e6babfc8db2")
    fun getMovieTopRated(): Observable<MovieListTopRateResponse>

    @GET("movie/{movie_id}?api_key=7211bbf86520c862f36e6e6babfc8db2")
    fun getMovieDetails(
        @Path("movie_id") MovieId: String
    ): Observable<MovieDetails>
}
