package com.farid.androidarch.apiHandler

import com.farid.androidarch.App
import com.farid.androidarch.BuildConfig
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit


/**
 * Created by Ahmed Moharm on 4/29/2018.
 */

class BaseApiHandler {

    companion object {
        private var retrofit: Retrofit? = null

        fun setupBaseApi(): Retrofit? {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(App.API_BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(provideOkHttpClient())
                    .build()
            }

            return retrofit
        }

        private const val CACHE_CONTROL = "Cache-Control"

        private fun provideOkHttpClient(): OkHttpClient {
            return OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor())
                .cache(provideCache())
                .build()
        }

        private fun provideCache(): Cache? {
            var cache: Cache? = null
            try {
                cache = Cache(
                    File(App.instance!!.cacheDir, "https-cache"),
                    10 * 1024 * 1024
                ) // 10 MB
            } catch (e: Exception) {

            }

            return cache
        }

        private fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
            val httpLoggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { })
            httpLoggingInterceptor.level =
                    if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.HEADERS else HttpLoggingInterceptor.Level.NONE
            return httpLoggingInterceptor
        }

        private fun provideCacheInterceptor(): Interceptor {
            return Interceptor { chain ->
                val response = chain.proceed(chain.request())

                // re-write response header to force use of cache
                val cacheControl = CacheControl.Builder()
                    .maxAge(2, TimeUnit.MINUTES)
                    .build()

                response.newBuilder()
                    .header(CACHE_CONTROL, cacheControl.toString())
                    .build()
            }
        }

        private fun provideOfflineCacheInterceptor(): Interceptor {
            return Interceptor { chain ->
                var request = chain.request()

                if (!App.hasNetwork()) {
                    val cacheControl = CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build()

                    request = request.newBuilder()
                        .cacheControl(cacheControl)
                        .build()
                } else {

                }

                chain.proceed(request)
            }
        }

    }
}
