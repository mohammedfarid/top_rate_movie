package com.farid.androidarch.model

data class ProductionCountriesItem(
	val iso_3166_1: String? = null,
	val name: String? = null
)
