package com.farid.androidarch.model

data class SpokenLanguagesItem(
	val name: String? = null,
	val iso_639_1: String? = null
)
