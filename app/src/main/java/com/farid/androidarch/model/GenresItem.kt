package com.farid.androidarch.model

data class GenresItem(
	val name: String? = null,
	val id: Int? = null
)
