package com.farid.androidarch.model

data class GenersResponse(
	val genres: List<GenresItem?>? = null
)
